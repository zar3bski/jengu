# jengu

![](https://gitlab.com/zar3bski/jengu/badges/master/pipeline.svg) 
![](https://gitlab.com/zar3bski/jengu/badges/master/coverage.svg)

ERP for independent therapist

## Usage

## Testing and development

### Requirements

* [docker](https://docs.docker.com/install/linux/docker-ce/debian/)
* [docker-compose](https://docs.docker.com/compose/install/)


clone this repository
```
git clone https://gitlab.com/zar3bski/jengu.git
cd jengu
```

run the application in development mode using the provided `docker-compose.yml`
```
docker-compose up -d --build
```

default credentials and users: 


|            | john (**admin**)|  paul    |
|-----------:|:----------------|:---------|
|password    | 478951236a      | 698753214a|
|private key | `OSEuOrw7BDANm2b0lwddBXUxN6OFGBLBDoFbqnkdMNU=` | `ry860ekZ8T1UDTzvoPSlAVMEOjcVz3ODLYbjXfySns0=`|
|public key  | `bQNbTjHETLTc/RNJYa1mTDg0fQF70GsuIZFsrb43DQc=` | `G8608AL7TE2n3P10OLS8V/8wCaf/mzflCS/5qw/TzG4=` |


## add environement files for production

You will need two files at the root of the project: 

**.env**

```
DEBUG=0
SECRET_KEY=some_secret_key_of_your_choice
STATIC_FILES_HOST=/usr/local/share/jengu/staticfiles  # leave it this way or change the proxy conf accordingly
SQL_ENGINE=django.db.backends.postgresql
SQL_DATABASE=some_db_name
SQL_USER=some_user_name
SQL_PASSWORD=some_looooooooong_password
SQL_HOST=db
SQL_PORT=5432
DATABASE=postgres
```
**.env.db**

```
POSTGRES_USER=some_user_name
POSTGRES_PASSWORD=some_looooooooong_password
POSTGRES_DB=some_db_name
```


### Set an Apache Proxy Pass up

Enable the following modules
```
a2enmod proxy proxy_http
```

use the following conf: 

```
<VirtualHost *:80>
	ServerName jengu.likeitmake.it	
	
	Alias /staticfiles /usr/local/share/jengu/staticfiles
	ProxyPass /staticfiles !	
	ProxyPass / http://localhost:8000/
		
	<Directory /usr/local/share/jengu/staticfiles>
                Options Indexes FollowSymLinks
                Order allow,deny
                Allow from all
                Require all granted
    </Directory>

    SetEnvIf Request_URI "^/staticfiles/.*" dontlog

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined env=!dontlog
		
</VirtualHost>
```

### Add a Fail2ban to prevent bruteforce attacks

If your server uses [Fail2ban](https://www.fail2ban.org/wiki/index.php/Main_Page), provided that Apache is writing its access logs in `/var/log/apache*/*access.log` , you can easily prevent bruteforce attacks on login urls with the two following steps. 

**create a filter in `/etc/fail2ban/filter.d/auth-jengu.conf`**

```
[INCLUDES]
before = common.conf
[Definition]
failregex =<HOST> - -.*POST (/admin/login/|/jengu/login).*
```

**create a jail in `/etc/fail2ban/jail.d/some-configuration.conf`** Add the following rule before you restart fail2ban

```
[jengu-auth]
enabled  = true
findtime = 600
bantime  = 86400
port     = http,https
filter   = auth-jengu
logpath  = /var/log/apache*/*access.log
maxretry = 5
```


Live long and prosper \\//_
