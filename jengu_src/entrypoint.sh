#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

python manage.py migrate

if [ "$PIPENV_DEV" = true ]
then 
	python manage.py flush --no-input
	python manage.py loaddata /usr/src/jengu_test/fixtures/*
fi


python manage.py collectstatic --no-input --clear

if [ "$PIPENV_DEV" = true ]
then 
	python /usr/src/jengu/manage.py runserver 0.0.0.0:8000
else
	gunicorn jengu.wsgi:application --bind 0.0.0.0:8000
fi

exec "$@"