from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
import base64

def is_base64(value): 
    try: 
        assert base64.b64encode(base64.b64decode(value)) == bytes(value.encode('UTF-8'))
        return value
    except: 
        raise ValidationError(_("{} was not encrypted".format(value)),
            params={'value': value},) 
        