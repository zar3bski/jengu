from django.test import TestCase, RequestFactory
from django.test import Client as WebClient
from .models import *
from .views import *
from .forms import *
from django.conf import settings
from django.contrib.auth.models import User, AnonymousUser
import base64
import nacl.utils
from nacl.public import Box, PrivateKey, PublicKey
from nacl.exceptions import CryptoError
from datetime import timedelta
from django.db.utils import IntegrityError
from django.core.exceptions import ValidationError
from django.contrib.messages import get_messages

# NB: personnal informations are crypted
JOHN_PRIVATE = "OSEuOrw7BDANm2b0lwddBXUxN6OFGBLBDoFbqnkdMNU="
JOHN_PUBLIC = "bQNbTjHETLTc/RNJYa1mTDg0fQF70GsuIZFsrb43DQc="

PAUL_PRIVATE = "ry860ekZ8T1UDTzvoPSlAVMEOjcVz3ODLYbjXfySns0="
PAUL_PUBLIC = "G8608AL7TE2n3P10OLS8V/8wCaf/mzflCS/5qw/TzG4="


def base64_to_bytes(key: str) -> bytes:
    return base64.b64decode(key.encode('utf-8'))


def encrypt_for_user(sender_private: str, receiver_public: str, message: str) -> str:
    sender_box = Box(PrivateKey(base64_to_bytes(sender_private)),
                     PublicKey(base64_to_bytes(receiver_public)))
    return base64.b64encode(sender_box.encrypt(bytes(message, "utf-8"))).decode('utf-8')


def decrypt_for_user(receiver_private: str, sender_public: str, message: str) -> str:
    receiver_box = Box(PrivateKey(base64_to_bytes(receiver_private)),
                       PublicKey(base64_to_bytes(sender_public)))
    return receiver_box.decrypt(base64.b64decode(message.encode('utf-8'))).decode('utf-8')

# TEST MODELS


class TestClientModel(TestCase):
    fixtures = ['clients.yaml', 'users.yaml']

    def test_if_non_legal_representative_required(self):
        """If someone is not legally competent, she should have a legal representative"""
        with self.assertRaises(IntegrityError):
            c = Client.objects.get(pk=2)
            c.is_legal = False
            c.save()

    def test_non_base64_raise_exception(self):
        values_dict = {"fk_owner": User.objects.get(pk=1),
                       "first_name": "dGVzdA==",
                       "last_name": "dGVzdA==",
                       "birth_date": "2000-01-01",
                       "phone": "dGVzdA==",
                       "mail": "dGVzdA==",
                       "notes": "dGVzdA==",
                       "status": "active",
                       "is_legal": False,
                       "legal_representative": "dGVzdA=="}
        for key in ["first_name", "last_name", "phone", "mail", "notes", "legal_representative"]:
            with self.assertRaises(ValidationError):
                c = Client(**{**values_dict, key: "éééééééééééé"})
                c.clean_fields()

    def test_decrypt_retreived_(self):
        some_client = Client.objects.get(pk=2)
        self.assertEqual(decrypt_for_user(
            JOHN_PRIVATE, JOHN_PUBLIC, some_client.first_name), "Friedrich")

    def test_decrypt_retreived_withgarbage_public(self):
        some_client = Client.objects.get(pk=2)
        with self.assertRaises(CryptoError):
            self.assertEqual(decrypt_for_user(
                PAUL_PRIVATE, JOHN_PUBLIC, some_client.first_name), "Friedrich")

    def test_remove_user_remove_related_clients(self):
        """removing a user should also remove his patients"""
        User.objects.get(pk=2).delete()
        with self.assertRaises(Client.DoesNotExist):
            self.assertTrue(Client.objects.get(pk=5) == "john")

    def test_str_format_for_client_rendering(self): 
        """Since client side decryption strongly rely on formating, changes might have side effects"""
        some_client = Client.objects.get(pk=2)
        self.assertEqual(str(some_client), "{}: {} {}".format(some_client.id, some_client.first_name, some_client.last_name) )


class TestConsultationModel(TestCase):
    fixtures = ['users.yaml', 'clients.yaml',
                'prestationtype.yaml', 'consultation.yaml']

    def test_remove_client_does_not_remove_consultations(self):
        Client.objects.get(pk=2).delete()
        self.assertTrue(Consultation.objects.get(pk=1).status == 'done')
        self.assertTrue(Consultation.objects.get(pk=3).fk_client.count() == 0)

    def test_remove_user_removes_his_consultations(self):
        """removing a user should also remove his consultations"""
        User.objects.get(pk=2).delete()
        with self.assertRaises(Consultation.DoesNotExist):
            self.assertTrue(Consultation.objects.get(pk=2).payed == 80)

    def test_missing_time_raise_exception(self):
        with self.assertRaises(IntegrityError):
            c = Consultation(fk_owner=User.objects.get(pk=1),
                             fk_type=PrestationType.objects.get(pk=1),
                             duration=timedelta(minutes=30),
                             payed=80,
                             status="done")
            c.save()
            c.fk_client.add(Client.objects.get(pk=1))

    def test_missing_duration_raise_exception(self):
        with self.assertRaises(IntegrityError):
            d = Consultation(fk_owner=User.objects.get(pk=1),
                             fk_type=PrestationType.objects.get(pk=1),
                             date="2013-03-16T17:41:28+00:00",
                             payed=80,
                             status="done")
            d.save()
            d.fk_client.add(Client.objects.get(pk=1))


# class TestPrestationTypeModel(TestCase):
#    pass

# FORM TESTS

class TestAddClientForm(TestCase):
    good_form = {"first_name": encrypt_for_user(JOHN_PRIVATE, JOHN_PUBLIC, "toto"),
                 "last_name": encrypt_for_user(JOHN_PRIVATE, JOHN_PUBLIC, "toto"),
                 "birth_date": "31/03/1989",
                 "phone": encrypt_for_user(JOHN_PRIVATE, JOHN_PUBLIC, "+336181796"),
                 "mail": encrypt_for_user(JOHN_PRIVATE, JOHN_PUBLIC, "some@mail.com"),
                 "notes": encrypt_for_user(JOHN_PRIVATE, JOHN_PUBLIC, "notes"),
                 "is_legal": False,
                 "legal_representative": encrypt_for_user(JOHN_PRIVATE, JOHN_PUBLIC, "john doe")}

    def test_minimal_information(self):
        form = AddClientForm({"first_name": encrypt_for_user(JOHN_PRIVATE, JOHN_PUBLIC, "toto"),
                              "last_name": encrypt_for_user(JOHN_PRIVATE, JOHN_PUBLIC, "toto"),
                              "birth_date": "31/03/1989",
                              "is_legal": True})
        self.assertTrue(form.is_valid())

    def test_maximal_information(self):
        form = AddClientForm(self.good_form)
        self.assertTrue(form.is_valid())

    def test_non_encrypted_information_generate_errors(self):
        form = AddClientForm({**self.good_form, "first_name": "Chenavard", "mail": "some@mail.com"})
        self.assertFalse(form.is_valid())
        error_messages = [x[1][0] for x in form.errors.items()]
        self.assertEqual(len(error_messages), 2)
        self.assertTrue("Chenavard was not encrypted" in error_messages)
        self.assertTrue("some@mail.com was not encrypted" in error_messages)

    def test_missing_values(self):
        form = AddClientForm(
            {i: j for i, j in self.good_form.items() if i != 'last_name'})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'last_name': [
                         'This field is required.']})

class TestRecordConsultationForm(TestCase):
    fixtures = ['users.yaml', 'clients.yaml',
                'consultation.yaml', 'prestationtype.yaml']

    def test_option_client_have_class_encrypted(self): 
        form = RecordConsultationForm()
        #TODO

    def test_submit_valid_form(self):
        form = RecordConsultationForm({
            "fk_type": PrestationType.objects.get(denomination="hypnosis").pk,
            "fk_client": [Client.objects.get(pk=1).pk],
            "duration": 360,
            "date": "14/01/2020 15:20"}, user=User.objects.get(pk=1))
        self.assertTrue(form.is_valid())

# VIEW TEST
class GeneralViewTest:

    def test_authenticated_user_can_access(self):
        c = WebClient()
        c.login(username='john', password='478951236a')
        response = c.get(self.request.path)
        self.assertEqual(response.status_code, 200)

    def test_anonymous_user_is_redirected(self):
        c = WebClient()
        response = c.get(self.request.path, follow=True)
        self.assertRedirects(response,
                             "{}?next={}".format(
                                 settings.LOGIN_URL, self.request.path),
                             status_code=302,
                             target_status_code=200,
                             msg_prefix='',
                             fetch_redirect_response=True)


class TestHomeView(TestCase, GeneralViewTest):
    fixtures = ['users.yaml']

    def setUp(self):
        self.request = RequestFactory().get('/console/')
        self.request.user = User.objects.get(pk=1)


class TestBrowseClientsView(TestCase, GeneralViewTest):
    fixtures = ['users.yaml', 'clients.yaml']

    def setUp(self):
        self.request = RequestFactory().get('/console/clients/')
        self.request.user = User.objects.get(pk=1)
        self.view = BrowseClients()

    def test_return_active_clients_ordered_by_default(self):
        self.view.setup(self.request)
        context = self.view.get_context_data()
        self.assertEqual([x.id for x in context["item_list"]], [3, 2, 1])

    def test_return_clients_ordered_by_query_set(self):
        request = RequestFactory().get('/console/clients/?order_by=birth_date')
        request.user = User.objects.get(pk=1)
        view = BrowseClients()
        view.setup(request)
        context = view.get_context_data()
        self.assertEqual([x.id for x in context["item_list"]], [2, 3, 1])

    def test_return_clients_client_by_status(self):
        request = RequestFactory().get('/console/clients/?status=active&order_by=birth_date')
        request.user = User.objects.get(pk=1)
        view = BrowseClients()
        view.setup(request)
        self.view.setup(request)
        context = self.view.get_context_data()
        self.assertEqual([x.id for x in context["item_list"]], [2, 3, 1])

        request = RequestFactory().get('/console/clients/?status=former&order_by=birth_date')
        request.user = User.objects.get(pk=1)
        view = BrowseClients()
        view.setup(request)
        self.view.setup(request)
        view.setup(request)
        context = view.get_context_data()
        self.assertEqual([x.id for x in context["item_list"]], [4])

    def test_post_valid_create_client(self):
        c = WebClient()
        c.login(username='john', password='478951236a')
        response = c.post('/console/clients/',
                          TestAddClientForm.good_form, follow=True)
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'Recorded in active')
        created_client = Client.objects.order_by('-id')[0]
        self.assertEqual(decrypt_for_user(JOHN_PRIVATE, JOHN_PUBLIC, created_client.first_name), "toto")
        self.assertEqual(created_client.status, 'active')
        self.assertEqual(created_client.fk_owner, User.objects.get(pk=1))

    def test_post_valid_client_status_depends_on_the_context(self):
        c = WebClient()
        c.login(username='john', password='478951236a')
        response = c.post('/console/clients/?status=former',
                          TestAddClientForm.good_form, follow=True)
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'Recorded in former')
        created_client = Client.objects.order_by('-id')[0]
        self.assertEqual(created_client.status, 'former')

    def test_post_non_encrypted_client(self):
        c = WebClient()
        c.login(username='john', password='478951236a')
        response = c.post('/console/clients/', {**TestAddClientForm.good_form, "phone": "+33899815", "first_name": "Paul-Marc-Joseph"}, follow=True)
        messages = [str(x) for x in list(response.context['messages'])]
        self.assertEqual(len(messages), 2)
        self.assertTrue(
            'Invalid first_name: Paul-Marc-Joseph was not encrypted' in messages)
        self.assertTrue(
            'Invalid phone: +33899815 was not encrypted' in messages)


class TestBrowseConsultationsView(TestCase, GeneralViewTest):
    fixtures = ['users.yaml', 'clients.yaml',
                'consultation.yaml', 'prestationtype.yaml']

    def setUp(self):
        self.request = RequestFactory().get('/console/consultations/')
        self.request.user = User.objects.get(pk=1)
        self.view = BrowseConsultations()

    def test_form_queryset_client_only_include_those_of_the_user(self): 
        self.view.setup(self.request)
        context = self.view.get_context_data() 
        self.assertFalse("i1/xx+wK8eoUOfw2ztBGtruigtWEoSHRIwi/njaVT9Wh/eeH3PuejeDg/qqAo6LBbzSMQw==" in str(context["form"]["fk_client"]))

    def test_return_booked_consultations_ordered_by_default(self):
        self.view.setup(self.request)
        context = self.view.get_context_data()
        self.assertEqual([x.id for x in context["item_list"]], [8])

    def test_return_consultattion_by_status_and_ordered_null_first(self):
        request = RequestFactory().get('/console/clients/?status=done&order_by=payed')
        request.user = User.objects.get(pk=1)
        self.view.setup(request)
        context = self.view.get_context_data()
        self.assertEqual([x.id for x in context["item_list"]], [7, 1, 3, 5])
        self.assertEqual(context["item_list"]
                         [1].fk_type.denomination, 'hypnosis')
        self.assertEqual(decrypt_for_user(JOHN_PRIVATE, JOHN_PUBLIC,
                                          context["item_list"][1].fk_client.all()[0].first_name), "Sigmund")

    def test_port_valid_record_form_records_and_notify(self):
        c = WebClient()
        c.login(username='john', password='478951236a')
        response = c.post('/console/consultations/',
                          {"fk_type":2, "fk_client":2, "duration": 240, "date": "01/09/2020 16:58"}, follow=True)
        # messaging
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'Recorded in booked')

        # record
        created_consultation = Consultation.objects.order_by('-id')[0]
        self.assertEqual(created_consultation.fk_type, PrestationType.objects.get(pk=2))
        self.assertEqual(created_consultation.duration, timedelta(seconds=240))

    def test_wrong_user_cant_record_client_of_other(self):
        c = WebClient()
        c.login(username='paul', password='698753214a')
        response = c.post('/console/consultations/',
                          {"fk_type":2, "fk_client":2, "duration": 300, "date": "01/09/2020 16:58"}, follow=True)

        # messaging
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'Invalid fk_client: Select a valid choice. 2 is not one of the available choices.')

        # record
        created_consultation = Consultation.objects.order_by('-id')[0]
        self.assertFalse(created_consultation.fk_type == PrestationType.objects.get(pk=2))
        self.assertFalse(created_consultation.duration == timedelta(seconds=300))

class TestClientDetails(TestCase, GeneralViewTest):
    fixtures = ['users.yaml', 'clients.yaml',
                'consultation.yaml', 'prestationtype.yaml']

    def setUp(self):
        self.request = RequestFactory().get('/console/client/2')
        self.request.user = User.objects.get(pk=1)
        self.view = ClientDetails()

    def test_get_consultations_history_of_client(self): 
        self.view.setup(self.request, pk=2)
        context = self.view.get_context_data()
        self.assertEqual(context["client"], Client.objects.get(pk=2))
        self.assertTrue(len(context["consultations"]) == 2)
        self.assertTrue([x.id for x in context["consultations"]] == [1,3])

    def test_initial_values_of_form_match(self): 
        self.view.setup(self.request, pk=2)
        context = self.view.get_context_data()
        form = context["form"]
        self.assertTrue(context["client"].first_name in str(form))
        self.assertTrue(context["client"].last_name in str(form))
        self.assertTrue(context["client"].phone in str(form))
        self.assertTrue(context["client"].mail in str(form))
        self.assertTrue(context["client"].notes in str(form))

class TestClientEdit(TestCase, GeneralViewTest): 
    fixtures = ['users.yaml', 'clients.yaml',
                'consultation.yaml', 'prestationtype.yaml']

    def setUp(self):
        self.request = RequestFactory().post('/console/client/2/edit')
        self.request.user = User.objects.get(pk=1)
        self.view = ClientEdit()

    def test_post_valid_changes(self): 
        c = WebClient()
        c.login(username='john', password='478951236a')
        response = c.post('/console/client/2/edit',
                          TestAddClientForm.good_form, follow=True)

        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), "Successfully updated")
        self.assertEqual(decrypt_for_user(JOHN_PRIVATE,JOHN_PUBLIC,Client.objects.get(pk=2).first_name), "toto")

    def test_post_invalid_changes(self): 
        c = WebClient()
        c.login(username='john', password='478951236a')
        response = c.post('/console/client/2/edit',
                       {"first_name": "something",
                       "last_name": "dGVzdA==",
                       "birth_date": "01/01/2001",
                       "phone": "dGVzdA==",
                       "mail": "dGVzdA==",
                       "notes": "dGVzdA==",
                       "status": "active",
                       "is_legal": False,
                       "legal_representative": "dGVzdA=="}, follow=True)

        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), "Invalid first_name: something was not encrypted")

    def test_post_changes_wrong_user(self): 
        c = WebClient()
        c.login(username='paul', password='698753214a')
        response = c.post('/console/client/2/edit',
                          TestAddClientForm.good_form, follow=True)

        self.assertEqual(response.status_code, 403)

class TestConsultationEdit(TestCase): 

    fixtures = ['users.yaml', 'clients.yaml',
                'consultation.yaml', 'prestationtype.yaml']

    def setUp(self):
        self.request = RequestFactory().post('/console/consultation/6/edit')
        self.request.user = User.objects.get(pk=1)
        self.view = ConsultationEdit()

    def test_post_valid_changes(self): 
        c = WebClient()
        c.login(username='paul', password='698753214a')

        response = c.post('/console/consultation/6/edit',
            {"fk_type": 2,
            "duration": 600,
            "date": "01/09/2020 16:58",
            "payed": 60,
            "status": "done"}, follow=True)
        # TODO
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), "Consultation 6 successfully updated")
        self.assertEqual(Consultation.objects.get(pk=6).status, "done")
        self.assertEqual(Consultation.objects.get(pk=6).payed, 60)
        self.assertEqual(Consultation.objects.get(pk=6).fk_owner, User.objects.get(username="paul"))

    def test_non_authenticated_cant_alter(self):
        c = WebClient()
        response = c.post('/console/consultation/6/edit',
            {"fk_type": 2,
            "duration": 600,
            "date": "01/09/2020 16:58",
            "payed": 60,
            "status": "done"})

        self.assertFalse(Consultation.objects.get(pk=6).status == "done")
        self.assertFalse(Consultation.objects.get(pk=6).payed == 60)