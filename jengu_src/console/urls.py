from django.urls import path
from django.contrib.auth import views as auth_views
from .views import *

urlpatterns = [
	path('', Home.as_view(), name='home'),
	path('login/', auth_views.LoginView.as_view(template_name='console/login.html'), name='login'),
	path('logout/', logout_view, name='logout'),
	path('clients/', BrowseClients.as_view(), name='browse_clients'),
	path('client/<int:pk>', ClientDetails.as_view(), name='client_details'),
	path('client/<int:pk>/edit', ClientEdit.as_view(), name='client_edit'),
	path('consultations/', BrowseConsultations.as_view(), name='browse_consultations'),
	path('consultation/<int:pk>/edit', ConsultationEdit.as_view(), name='edit_consultation'),
]