
function storePass() {
    window.sessionStorage.pass = base64js.fromByteArray(nacl.hash(new TextEncoder("utf-8").encode(document.getElementById("id_password"))).slice(0,32));
}

/*store private key, encrypted with pass*/
function encryptAndStore(key_b64) {
  var nonce = nacl.randomBytes(24);
  var box = nacl.secretbox(base64js.toByteArray(key_b64), nonce, 
        base64js.toByteArray(window.sessionStorage.pass));
  window.localStorage.setItem('jengu.priv.storage', 
      padWithNonce(nonce,box));
}

function dropSessionInformation() {
    window.sessionStorage.removeItem("public_key");
    window.sessionStorage.removeItem("private_key");
    window.sessionStorage.removeItem("pass");
}

function decryptPrivateKey() {
    var message = base64js.toByteArray(window.localStorage.getItem('jengu.priv.storage'));
    var nonce = message.slice(0, 24);
    var message = message.slice(24, message.length);
    var secret = nacl.secretbox.open(message, nonce, base64js.toByteArray(sessionStorage.getItem("pass")));
    window.sessionStorage.setItem("public_key", base64js.fromByteArray(nacl.box.keyPair.fromSecretKey(secret).publicKey));
    window.sessionStorage.setItem("private_key", base64js.fromByteArray(secret));
}

function padWithNonce(nonce_array,box_array) {
  var encrypted = new Uint8Array(nonce_array.length + box_array.length);
    encrypted.set(nonce_array);
    encrypted.set(box_array, nonce_array.length);
    return base64js.fromByteArray(encrypted);
}

function encrypt(message) {
    var public = base64js.toByteArray(sessionStorage.getItem("public_key"));
    var private = base64js.toByteArray(sessionStorage.getItem("private_key"));
    var message = new TextEncoder("utf-8").encode(message);
    var nonce = nacl.randomBytes(24);
    var box = nacl.box(message, nonce, public, private);
    return padWithNonce(nonce, box);
}

function decrypt(message) {
    var public = base64js.toByteArray(sessionStorage.getItem("public_key"));
    var private = base64js.toByteArray(sessionStorage.getItem("private_key"));
    var message = base64js.toByteArray(message)
    var nonce = message.slice(0, 24);
    var message = message.slice(24, message.length);
    return new TextDecoder("utf-8").decode(nacl.box.open(message, nonce, public, private));
}

function decrypt_form(form) {
    len = document.getElementById(form).options.length

    for (i = 1; i < len; i++) {
        var fields = document.getElementById(form).options[i].text;
        fields = fields.split(', ');
        var date = fields[2];

        fields = fields.slice(0, 2).map(t => decrypt(t));
        fields.push(date);
        fields = fields.join(", ");

        document.getElementById(form).options[i].text = fields;
    }
}


function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;
    // CSV FILE
    csvFile = new Blob([csv], { type: "text/csv" });
    // Download link
    downloadLink = document.createElement("a");
    // File name
    downloadLink.download = filename;
    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);
    // Make sure that the link is not displayed
    downloadLink.style.display = "none";
    // Add the link to your DOM
    document.body.appendChild(downloadLink);
    // Lanzamos
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("table tr");

    for (var i = 0; i < rows.length; i++) {
        var row = [],
            cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++)
            row.push(cols[j].innerText);

        csv.push(row.join(","));
    }
    // Download CSV
    download_csv(csv.join("\n"), filename);
}


$(document).ready(function() {
    $( "span[id*='encrypted']" ).each(function(span, index) {
        try {
            index.firstChild.data = decrypt(index.firstChild.data);
        }catch{
            console.log("could not decrypt content");
        }
    });

    $( ".2encrypt" ).map(function(){
        try {
            $(this).val(decrypt($(this).val()));
        }catch{
            console.log("could not decrypt content");
        }
    });

    $( "form" ).submit(function() {
        $(this).find("input").filter(".2encrypt").map(function(){
            $(this).val(encrypt($(this).val()));
        });
        $(this).find("textarea").filter(".2encrypt").map(function(){
            $(this).val(encrypt($(this).val()));
        });
    });

    $('.panel-tabs > a').click(function(event){
        var active_tab_selector = $(this).parents('.panel-tabs').parents().children('.panel-block').filter(".is-active");
        event.preventDefault();                 
        
        //find actived navigation and remove 'active' css
        var actived_nav = $(this).parents('.panel-tabs').filter(".is-active");
        //console.log(actived_nav);
        actived_nav.removeClass('is-active');
                    
        //add 'active' css into clicked navigation
        $(this).parents('li').addClass('is-active');
                    
        //hide displaying tab content
        $(active_tab_selector).removeClass('is-active');
        $(active_tab_selector).addClass('is-hidden');
                    
        //show target tab content
        var target_tab_selector = $(this).attr('href');
        $(target_tab_selector).removeClass('is-hidden');
        $(target_tab_selector).addClass('is-active');
    });

    
    $( "select#id_fk_client > option" ).map(function() {
        var fields = $(this).text().split(/[\s:]+/); 
        $(this).text(`${fields[0]} - ${decrypt(fields[1])} ${decrypt(fields[2])}`) 
    });
    $(".modal-button").click(function() {
      var target = $(this).data("target");
      $("html").addClass("is-clipped");
      $(target).addClass("is-active");
    });

    $(".modal-close").click(function() {
      $("html").removeClass("is-clipped");
      $(this).parent().removeClass("is-active");
    });
    $("#registerKey").click(function() {
        encryptAndStore($(this).parent("form").find("#inputKey").val());
        decryptPrivateKey()
    });
});
